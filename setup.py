from setuptools import setup


setup(
      name="CoGrove",
      version="0.1.0",
      packages=["cogrove"],
      url="https://bitbucket.org/protocypher/ma-cogrove",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A company heirarchy application."
)

