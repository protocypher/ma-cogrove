# CoGrove

**Company Hierarchy**

CoGrove is a basic, text based, company hierarchy application that uses the
`cmd.Cmd` framework. It manages different classes of employees, whether they
have and can have direct reports, and the company's overall heirarchy.

